import React,{ useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { useQuery, gql } from "@apollo/client";
import backArrow from "./../../assest/icons/png/back-arrow-48.png";
import MapContainer from "./../../components/Map/index";
import { Geocode } from "./../../service";

function Details() {

    const { id } = useParams();
 
    function DetailsCountry(){
        const [goecodeLatLng, setGoecodeLatLng] = useState([]);
        const [markerCountry, setMarkerCountry] = useState([]); 
        const [loadMap, setLoadMap] = useState(true);
        const strokeColor = ['#6A5ACD', '#B8860B', 	'#DC143C', '#FF8C00', '#006400']
        const { loading, error, data } = useQuery(gql`{
            Country(_id:"${id}") {
                flag{
                  svgFile
                }
                name
                capital
                area
                population
                topLevelDomains {
                  _id
                  name
                }
                distanceToOtherCountries(first: 5){
                    distanceInKm
                    countryName
                }
            }
        }`);

        useEffect(()=>{
            if(data){
                const {name, distanceToOtherCountries = []} = data.Country[0];
                getCountryMake(name).finally(()=>{
                    getGeocodeAll(distanceToOtherCountries).finally(()=>{
                        setLoadMap(false);
                    })
                });
            }
        },[data]);

        async function getCountryMake(name) {
            const data = await Geocode.searchGeo(name);
            const {geometry: {location}} = data.results[0];
            setMarkerCountry(location);
        }

        async function getGeocodeAll(distanceToOtherCountries) {
            const data = await Promise.all(
                distanceToOtherCountries.map(({countryName}) => {
                    countryName = countryName.split(' (')[0]
                    return Geocode.searchGeo(countryName)})
            )
            .then((result) => {
                return result.map(e => e.results[0].geometry.location)
            })
            .catch((error) => {
                console.log(error);
            });
            setGoecodeLatLng([...goecodeLatLng, ...data]);
        }


        if (loading) return <p>Loading...</p>;
        if (error) return <p>Error :(</p>;

        const { flag, area, capital, name, population,  topLevelDomains, distanceToOtherCountries} = data.Country[0];

        return (
            <div className='flex-row'>
                <ul className='flex-item-4'>
                    <li>
                        <b>País:</b>
                        <span>{name}</span>
                    </li>
                    <li>
                        <b>Capital:</b>
                        <span>{capital}</span>
                    </li>
                    <li>
                        <b>Área:</b>
                        <span>{area}</span>
                    </li>
                    <li>
                        <b>População:</b>
                        <span>{population}</span>
                    </li>
                    <li>
                        <b>TLD:</b>
                        <span>{topLevelDomains[0].name}</span>
                    </li>
                    <li><img src={flag.svgFile}/></li>
                    <li>
                        <b>Países próximos:</b>
                        <span>{distanceToOtherCountries.length}</span>
                    </li>
                    {
                        distanceToOtherCountries.map(({countryName, distanceInKm}, index) =>
                            (<li key={countryName}>
                                <b style={{color: strokeColor[index]}}>{countryName}</b>
                                <span>{distanceInKm}</span>
                            </li>)
                        )
                    }
                </ul>   
                <article className='flex-item-8'>
                    {
                        loadMap ? (<p>Loading...</p>):
                        (<MapContainer marker={markerCountry} data={goecodeLatLng} strokeColor={strokeColor}/>)
                    }
                </article>
            </div>
        )
    }


    return (
        <section className='countries-details flex-item-12'>
            <header>
                <Link to={'/'}><img src={backArrow}/></Link>
                <h1>Detalhes</h1>
            </header>
            <DetailsCountry />
        </section>
    );
}

export default Details;