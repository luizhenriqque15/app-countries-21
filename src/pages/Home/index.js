import React,{  } from "react";
import {Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Countre from "../Country";
import Details from "../Details";

function Home() {
    return (
        <Router>
            <Switch>
                <Route exact path='/' component={Countre} />
                <Route exact path='/details/:id' component={Details} />
            </Switch>
        </Router>
    );
}

export default Home;