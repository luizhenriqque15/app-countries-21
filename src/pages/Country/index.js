import React,{ useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useQuery, useLazyQuery, useApolloClient, gql } from "@apollo/client";

function Countre() {

    const [countries, setCountries] = useState([]);
    const [load, setLoad] = useState(true);
    const [isError, setIsError] = useState(false);

    const getCountries = useQuery(gql`{
        Country{
            flag{
              emoji
            }
            _id
            name
            capital
        }
    }`);

    useEffect(() => {
       const { data, loading, error } = getCountries;

       setLoad(loading);
       if(data != undefined) setCountries(data.Country);
       if(error === undefined) setIsError(error)
    },[getCountries]);

    useEffect(()=>{
    },[])

    function ListCountries ({data}) {
        if (load) return <p>Loading...</p>;
        if (isError) return <p>Error :(</p>;

        return (
            <ul className='flex-row'>
                {
                    data.map(({flag: {emoji}, _id, name, capital}) => (
                        <li className='flex-item-3' key={_id}> 
                            <Link to={`/details/${_id}`}>
                                {/*<img className='flex-item-1' src={svgFile}/>*/}
                                <span role="img" aria-label="sheep">{emoji}</span>
                                <header>
                                    <h3>{name}</h3>  
                                    <span>{capital}</span>
                                </header>
                            </Link>
                        </li>
                    ))
                }
            </ul>
        )
    }

    function SearchCountries({ onChange }) {
        const [search, setSearch] = useState('');

        const SEARCH_COUNTRUIES = gql`{
                query(filter: _CountryFilter) {
                    getData(filter: filter){
                        flag{
                            emoji
                        }
                        _id
                        name
                        capital
                    }
                }
        }`

        const [ executeSearch, {data: result}] = useLazyQuery(SEARCH_COUNTRUIES);

        useEffect(()=>{
            if(search.length > 0){
                const filter = {filter: {name_contains: search }};

                executeSearch({
                    variables: {filter},
                });
            }
        },[search]);

        return (
            <input value={search} onChange={$e => setSearch($e.target.value)}/>
        );
    }

    return (
        <section className='countries flex-item-12'>
            <header>
                <h1>Países</h1>
                {/*<SearchCountries onChange={ e => console.log(e)}/>*/}
            </header>
            <ListCountries data={countries}/>
        </section>
    );
}

export default Countre;