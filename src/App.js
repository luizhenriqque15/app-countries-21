
import { Provider } from "react-redux";
import { ApolloProvider } from '@apollo/client';

//Redux
import store from "./redux/store";

//AppollClient
import client from "./graphql-client";

import Home from './pages/Home';

function App() {
  return(
    <>
        <header className='header-navegation'>
            <h1>O Mundo em bandeiras</h1>
            <h2>Seja Bem-Vindo</h2>
        </header>
        <div className='flex'>
          <ApolloProvider client={client}>
            <Provider store={store}>
                <Home />
            </Provider>
          </ApolloProvider>
        </div>
    </>
  )
}

export default App;
