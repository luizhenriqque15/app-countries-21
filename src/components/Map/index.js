import React, { Component } from 'react';
import { Map, GoogleApiWrapper, Marker, Polyline } from 'google-maps-react';

const {
    REACT_APP_KEY_MAP
} = process.env;

const style = {
    width: '100%',
    height: '100%'
}

const containerStyle = {
    width: '50%',
    height: '50%'
}

class MapContainer extends Component {

    constructor(props) {
        super(props);
    }

    path = [
        this.props.marker
    ]

    render() {
        return (
    
          <Map
            style={style}
            containerStyle={containerStyle}
            google={this.props.google}
            zoom={4}
            initialCenter={this.path[0]}>
            { this.props.data ? (
                this.props.data.map((e, index) => {
                    return (
                        <Polyline key={index} path={[...this.path, ...[e]]} options={{ strokeColor: this.props.strokeColor[index] }} />
                    )
                })
            ): (null)}

                {this.props.marker ? (<Marker position={this.props.marker} />) : (null)}
          </Map>
    
        );
    }
}

export default GoogleApiWrapper( (props) => ({apiKey: REACT_APP_KEY_MAP}))(MapContainer);