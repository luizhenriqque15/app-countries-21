const {
    REACT_APP_GOOGLE_API_URL,
    REACT_APP_GOOGLE_API_KEY
} = process.env

const key = `&key=${REACT_APP_GOOGLE_API_KEY}`

async function  searchGeo(params) {
    try {
        let data = await fetch(`${REACT_APP_GOOGLE_API_URL}/geocode/json?address=${params}${key}`);
        return await data.json();
    } catch (error) {
        throw new Error(error);
    }
}


export {
    searchGeo
}