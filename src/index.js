import React from 'react';
import ReactDOM from 'react-dom';

//Style
import './styles/index.css';
import './styles/flex.css';

//Components
import App from './App';

ReactDOM.render(<App/>, document.getElementById('root')
);